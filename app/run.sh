
#!/bin/bash

cd /var/local/app/

./.venv/bin/activate

export PYTHONPATH=$PYTHONPATH:/var/local/app/

python lbe-snake-basket/main.py

deactivate
